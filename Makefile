all: heuristic

DisjointSets.o: DisjointSets.cc DisjointSets.hh
	g++ -O2 -std=c++11 -Wall -Werror -c DisjointSets.cc
	
heuristic.o: heuristic.cc
	g++ -O2 -std=c++11 -Wall -Werror -c heuristic.cc
	
heuristic: heuristic.o DisjointSets.o
	g++ -O2 -std=c++11 -Wall -Werror -o heuristic heuristic.o DisjointSets.o

