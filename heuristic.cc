/**
 * \author Alejandro Suarez Hernandez
 * \brief Heuristic for the TSP problem (only for complete graphs)
 */

#include <iostream>
#include <vector>
#include <algorithm>
#include <assert.h>
#include "DisjointSets.hh"
using namespace std;

typedef vector<int> Row;
typedef vector<Row> Graph; /** Adjacency matrix (with weights); */

struct Edge
{
  int origin, destination, weight;
  bool operator <(const Edge& edge) const {
    return weight < edge.weight;
  }
};

/**
 * At each step, we takes the edge with minimum weight. We take care of
 * selecting no more than 2 edges per-node (i.e. in a cycle, the degree of each
 * node is 2). Also we take care that the selected edge does not form a cycle
 * until the very last one. Uses disjoint sets structure to check for cycles
 * (nodes from the same connected component are in the same set), like in
 * Kruskal's algorithm.
 * 
 * \param edges vector with all the graph's edges
 * \param n number of nodes
 * \param total_cost Where the total cost of the cycle will be stored.
 * 
 * \return a sequence of nodes, in the same order in which
 * they are traversed in the calculated path
 */
vector<int> obtain_initial_solution(vector<Edge>& edges, int n,
    int& total_cost)
{
  int k = 0;
  vector<int> retval(n); /** Sequence of nodes */
  vector<int> degree(n); /** Degree of each node in the constructed path. */
  vector<const Edge*> chosen_edges(n);
  DisjointSets djs(n);
  sort(edges.begin(), edges.end()); /** Sort edges according to their weight. */
  total_cost = 0;
  for (const Edge& edge : edges)
  {
    bool eligible = degree[edge.origin] <= 1 and degree[edge.destination] <= 1;
    if (k < n-1)
      eligible = eligible and
        djs.find(edge.origin) != djs.find(edge.destination);
    if (eligible)
    {
      djs.join(edge.origin, edge.destination);
      chosen_edges[k++] = &edge;
      ++degree[edge.origin];
      ++degree[edge.destination];
      total_cost += edge.weight;
    }
    if (k == n) break;
  }
  retval[0] = chosen_edges[0]->origin;
  retval[1] = chosen_edges[0]->destination;
  /** Store sequence of nodes and return it. */
  int i = 2;
  while (i < n)
  {
    for (const Edge* edge : chosen_edges)
    {
      if (edge->origin == retval[i-1] and edge->destination != retval[i-2])
        retval[i++] = edge->destination;
      else if (edge->destination == retval[i-1] and edge->origin != retval[i-2])
        retval[i++] = edge->origin;
    }
  }
  return retval;
}

/** Reverses elements between i and j in vector v. Used in 2-opt. */
void reverse(vector<int>& v, int i, int j)
{
  int m = (i+j+1)/2;
  while (i < m) swap(v[i++],v[j--]);
}

/** Standard 2-opt algorithm */
void opt_2(const Graph& g, vector<int>& solution, int& total_cost) {
  int n = g.size();
  bool finished = false;
  while (not finished) {
    int best_inc = 0;
    int best_i = -1, best_j = -1;
    for (int i = 1; i < n; ++i)
    {
      for (int j = i+1; j < n; ++j)
      {
        int pre_u = solution[i-1];
        int post_v = j == n-1? solution[0] : solution[j+1];
        int u = solution[i];
        int v = solution[j];
        int inc = g[pre_u][v] + g[u][post_v] - g[pre_u][u] - g[v][post_v];
        if (inc < best_inc)
        {
          best_inc = inc;
          best_i = i;
          best_j = j;
        }
      }
    }
    if (best_inc < 0)
    {
      reverse(solution, best_i, best_j);
      total_cost += best_inc;
    }
    else finished = true;
  }
}

void print_vector(const vector<int>& v)
{
  int n = v.size();
  cout << v[0];
  for (int i = 1; i < n; ++i) cout << " " << v[i];
  cout << endl;
}

int main()
{
  /** Read graph from standard input (in adyacency matrix form).*/
  int n;
  cin >> n;
  
  assert(n > 0 and "The number of nodes must be positive");
  
  Graph g(n,Row(n));
  for (int u = 0; u < n; ++u)
    for (int v = 0; v < n; ++v)
      cin >> g[u][v];
  
  /** Put all the edges in a vector.*/
  vector<Edge> edges(n*(n-1)/2);
  int m = 0;
  for (int u = 0; u < n; ++u)
    for (int v = u + 1; v < n; ++v) {
      edges[m].origin = u;
      edges[m].destination = v;
      edges[m++].weight = g[u][v];
    }

  /** Calculate initial solution (constructive heuristic)*/
  int total_cost;
  vector<int> sol = obtain_initial_solution(edges,n,total_cost);
  
  /** Prints initial solution with its cost*/
  cout << "Initial solution:" << endl;
  print_vector(sol);
  cout << total_cost << endl;
  
  /** Applies improvement heuristic (with 2-OPT algorithm)*/
  opt_2(g, sol, total_cost);
  
  /** Print solution after improvement */
  cout << "Solution after 2-OPT:" << endl;
  print_vector(sol);
  cout << total_cost << endl;
}
