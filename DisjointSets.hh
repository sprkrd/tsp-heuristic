/**
 * \file DisjointSets.hh
 * \author Alejandro Suarez Hernandez
 * \brief disjoint sets.
 * 
 * Header of the DisjointSets class
 */

#ifndef _DISJOINTSETS_
#define _DISJOINTSETS_

#include <vector>

/**
 * \brief DisjointSets class
 * 
 * An instance of this class simbolizes n elements (represented by integers).
 * Each element is in a set. Each set is represented by a root element and may
 * contain many elements. However, two different sets may not share elements.
 * There are two basic operations: find, for finding the set of an element
 * (root of the set); join (union), for joining two different sets.
 */
class DisjointSets {
  private:
    /**
     *  roots_[i] stores the representative of element's i, or some negative
     *  integer if i is the representative itself.
     */
    std::vector<int> roots_;
    
    /** Number of different sets*/
    int nsets_;
    
  public:
    /**
     *  \brief Constructor
     *
     *  Creates an instance of this class with n elements and n sets (each one
     *  in its own set).
     * 
     *  \param n Number of elements and number of initial sets.
     */
    DisjointSets(int n);
    
    /**
     *  \brief Accessor
     *
     *  \return Number of different elements
     */
    inline int size() const {return roots_.size();}
    
    /**
     *  \brief Accessor
     * 
     *  \return number of sets
     */
    inline int nsets() const {return nsets_;}
    
    /**
     *  \brief Find root
     * 
     *  \param n The root of this element is returned.
     * 
     *  \return root or representative of element n (which may be n itself).
     */
    int find(int n);
    
    /**
     *  \brief Set joining
     * 
     *  Joins the sets to which u and v belong
     * 
     *  \param u first element
     *  \param v second element
     */
    void join(int u, int v);
};

#endif
