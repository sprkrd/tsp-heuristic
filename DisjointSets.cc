#include "DisjointSets.hh"

/**
 * \file DisjointSets.cc
 * \author Alejandro Suarez Hernandez
 * 
 * Implementation of the DisjointSets class
 */

DisjointSets::DisjointSets(int n) : roots_(n,-1), nsets_(n) {}

int DisjointSets::find(int u) {
  /** Path compression */
  if (roots_[u] >= 0) roots_[u] = find(roots_[u]);
  return roots_[u] < 0? u : roots_[u];
}

void DisjointSets::join(int u, int v) {
  int root1 = find(u);
  int root2 = find(v);
  if (root1 != root2) {
    /** Make use of height heuristic in order to minimize the time spent in
     * compressing paths and finding the roots_ of the elements. */
    int h1 = roots_[root1]; /** h1 = -height(root1) + 1*/
    int h2 = roots_[root2]; /** h2 = -height(root2) + 1*/
    if (h2 < h1) roots_[root1] = root2;
    else if (h1 < h2) roots_[root2] = root1;
    else {
      roots_[root1] = root2;
      roots_[root2] = h1 - 1;
    }
    --nsets_;
  }
}
