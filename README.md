# Heuristic for TSP in complete graphs.

The cycle is obtained as follows:

  * In a first stage, an initial solution is calculated. The strategy
  is to select the edges with less weight.
  * Then an improvement phase is applied. This phase consists of a simple
  2-OPT algorithm.

# To compile

Simply run `make`. C++11 features are used, so a compatible compiler is
required. Aside from this, there are not external dependencies.

# To run

Run `./heuristic < path/to/testcase`. For example, to execute it with the
provided test case, run `./heuristic < datos19.txt`.

Example output:

    Initial solution:
    0 3 5 2 8 10 4 1 7 9 6 11
    2477
    Solution after 2-OPT:
    0 8 10 4 1 6 9 7 3 5 2 11
    2301
